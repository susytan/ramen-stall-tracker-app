import 'package:flutter/material.dart';
import 'package:ramenstall/views/home.dart';
import 'package:ramenstall/views/stallMap.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    dynamic param = args;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Home());
      case '/home':
        return MaterialPageRoute(builder: (_) => Home());
      case '/stallMap':
        if (args != null) {
          return MaterialPageRoute(
            builder: (_) => StallMap(
                stallData: param['stallData'],
                initLatitude: param['initLatitude'],
                initLongitude: param['initLongitude']),
          );
        }
        return MaterialPageRoute(builder: (_) => Home());

      default:
        return MaterialPageRoute(builder: (_) => Home());
    }
  }
}
