import 'package:flutter/material.dart';

///Default Settings
///===================

final appName = "Ramen Stall Tracker";
final double defaultHeaderTextSize = 16;
final double defaultTextSize = 14;
final defaultTextColor = Colors.black;
final defaultHeaderTextColor = Colors.black;
final primaryColor = Colors.white;
final secondaryColor = const Color(0xffffde4d);
final disabledColor = Colors.black12;
final errorColor = Colors.red;
final successColor = Colors.green;

///CREDENTIALS
///===================
const String google_api_key = "AIzaSyCvg8V0dwSMgjZQhmCrJ01GizFXSWLdeyg";
const String reverseGeoCodingAPIUrl =
    "https://maps.googleapis.com/maps/api/geocode/json?";
const double mapZoom = 17.0;
