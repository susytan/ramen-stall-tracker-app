class StallModel {
  int id;
  String stallName;
  double longitude;
  double latitude;

  StallModel({
    this.id,
    this.stallName,
    this.longitude,
    this.latitude,
  });

  factory StallModel.fromJson(Map<String, dynamic> data) => new StallModel(
        id: data["id"],
        stallName: data["stallName"],
        longitude: data["longitude"],
        latitude: data["latitude"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "stallName": stallName,
        "longitude": longitude,
        "latitude": latitude,
      };

  StallModel.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.stallName = map['stallName'];
    this.latitude = map['latitude'];
    this.longitude = map['longitude'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this.id;
    map['stallName'] = stallName;
    map['longitude'] = longitude;
    map['latitude'] = latitude;
    return map;
  }
}
