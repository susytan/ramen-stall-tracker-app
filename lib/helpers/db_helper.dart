import 'package:path_provider/path_provider.dart';
import 'package:ramenstall/models/stallModel.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';

class DbHelper {
  static DbHelper _dbHelper;
  static Database _database;

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }

  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'stall.db';

    var database = openDatabase(path, version: 1, onCreate: _createDb);

    return database;
  }

  void _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE stall_data (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        stallName TEXT,
        latitude DOUBLE,
        longitude DOUBLE
      )
    ''');
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  //==========================================
  //Select All Data -> retiurn as List Map
  //==========================================
  Future<List<Map<String, dynamic>>> select() async {
    Database db = await this.database;
    var mapList = await db.query('stall_data', orderBy: 'id');
    return mapList;
  }
  //==========================================

  //==========================================
  //Select One Data
  //==========================================
  Future<List<Map<String, dynamic>>> detail(StallModel stall) async {
    Database db = await this.database;
    List<Map<String, dynamic>> data =
        await db.query('stall_data', where: 'id=?', whereArgs: [stall.id]);
    return data;
  }
  //==========================================

  //==========================================
  //Check Duplicate Data by Name
  //==========================================
  Future<int> checkDuplicate(String name) async {
    Database db = await this.database;
    List<Map<String, dynamic>> data =
        await db.query('stall_data', where: 'stallName=?', whereArgs: [name]);
    return data.length;
  }
  //==========================================

  //==========================================
  //Insert Data
  //==========================================
  Future<int> insert(StallModel object) async {
    Database db = await this.database;
    int count = await db.insert('stall_data', object.toMap());
    return count;
  }
  //==========================================

  //==========================================
  //Update Data
  //==========================================
  Future<int> update(StallModel object) async {
    Database db = await this.database;
    int count = await db.update('stall_data', object.toMap(),
        where: 'id=?', whereArgs: [object.id]);
    return count;
  }
  //==========================================

  //==========================================
  //Delete Data
  //==========================================
  Future<int> delete(int id) async {
    Database db = await this.database;
    int count = await db.delete('stall_data', where: 'id=?', whereArgs: [id]);
    return count;
  }
  //==========================================

  //==========================================
  //Get All Data -> return as List Model
  //==========================================
  Future<List<StallModel>> getStallList() async {
    var stallMapList = await select();
    int count = stallMapList.length;
    List<StallModel> stallList = List<StallModel>();
    for (int i = 0; i < count; i++) {
      stallList.add(StallModel.fromMap(stallMapList[i]));
    }
    return stallList;
  }
  //==========================================
}
