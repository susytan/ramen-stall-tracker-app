import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ramenstall/configs/routes.dart';
import 'package:ramenstall/configs/variables.dart';
import 'package:ramenstall/helpers/double_back_to_close.dart';
import 'package:ramenstall/views/home.dart';

void main() {
  runApp(MyApp());
}

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class MyApp extends AppMVC {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      theme: ThemeData(fontFamily: 'Lato'),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      navigatorKey: navigatorKey,
      onGenerateRoute: RouteGenerator.generateRoute,
      home: Scaffold(
        body: DoubleBackToCloseApp(child: Home()),
      ),
    );
  }
}
