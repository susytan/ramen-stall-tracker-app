import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ramenstall/configs/variables.dart';
import 'package:ramenstall/models/stallModel.dart';
import 'package:sqflite/sqflite.dart';
import 'package:dio/dio.dart';

class StallMapController extends ControllerMVC {
  factory StallMapController() {
    if (_this == null) _this = StallMapController._();
    return _this;
  }
  static StallMapController _this;

  StallMapController._();

  static StallMapController get con => _this;

  //==========================================
  //Get Address -> Get Address by Reverse Geocoding API
  //==========================================
  Future<String> getAddress(double lat, double long) async {
    String address = "";
    try {
      Dio dio = new Dio();
      dio.options.contentType = 'JSON';
      dio.options.responseType = ResponseType.json;
      String url = reverseGeoCodingAPIUrl +
          "latlng=" +
          lat.toString() +
          "," +
          long.toString() +
          "&key=" +
          google_api_key;
      Response<dynamic> response = await dio.get(url);

      address = response.data['results'][0]['formatted_address'];
      return address;
    } catch (e) {
      print(e);
      return address;
    }
  }
  //==========================================

  //==========================================
  //Get Stall Address -> Call Function Get Address
  //==========================================
  Future<String> getStallAddress(double lat, double long) async {
    String address = "";
    try {
      address = await StallMapController().getAddress(lat, long);
    } catch (e) {}

    return address;
  }
  //==========================================

  //==========================================
  //Home Page -> Redirect to Home Page (Page 1)
  //==========================================
  Future<dynamic> homePage(BuildContext context) async {
    final result = await Navigator.pushReplacementNamed(context, '/home');
    return result;
  }
}
