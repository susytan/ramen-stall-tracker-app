import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ramenstall/models/stallModel.dart';

class HomeController extends ControllerMVC {
  //==========================================
  //Variables
  //==========================================
  StallModel newStallData;
  bool serviceEnabled;
  PermissionStatus permissionGranted;
  LocationData locationData;
  Location location = new Location();
  LatLng _currentPosition = LatLng(0, 0);
  //==========================================

  //==========================================
  //Stall Map Page -> Go To Stall Map Page (Page 2)
  //==========================================
  Future<dynamic> stallMapPage(BuildContext context, StallModel stallData,
      double initLatitude, double initLongitude) async {
    dynamic args = ({
      "stallData": stallData,
      "initLatitude": initLatitude,
      "initLongitude": initLongitude
    });

    final result =
        await Navigator.pushNamed(context, '/stallMap', arguments: args);

    return result;
  }
  //==========================================

  //==========================================
  //Get Current Location -> get user location
  //==========================================
  Future<dynamic> getCurrentLocation() async {
    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    locationData = await location.getLocation();
    try {
      if (locationData != null) {
        _currentPosition =
            LatLng(locationData.latitude, locationData.longitude);
      }
      return _currentPosition;
    } catch (e) {
      return _currentPosition;
    }
  }
  //==========================================
}
