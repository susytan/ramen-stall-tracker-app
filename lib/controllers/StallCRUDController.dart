import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ramenstall/helpers/db_helper.dart';
import 'package:ramenstall/models/stallModel.dart';
import 'package:sqflite/sqflite.dart';

class StallCRUDController extends ControllerMVC {
  //==========================================
  //Variables
  //==========================================
  StallModel newStallData;
  String newStallName = "";
  DbHelper dbHelper = DbHelper();
  List<StallModel> stallList;
  bool serviceEnabled;
  PermissionStatus permissionGranted;
  LocationData locationData;
  Location location = new Location();
  LatLng currentPosition = LatLng(0, 0);
  //==========================================

  //==========================================
  //Get All Stall Record -> Get All Stall Data from database
  //==========================================
  Future<dynamic> getAllStallRecord() async {
    stallList = await dbHelper.getStallList();
    return stallList;
  }
  //==========================================

  //==========================================
  //Add Stall Record -> Insert New Data from database
  //==========================================
  Future<dynamic> addStallRecord(
      BuildContext context, String name, LatLng position) async {
    newStallName = name;

    newStallData = StallModel(
        stallName: newStallName,
        latitude: position.latitude,
        longitude: position.longitude);

    int result = await dbHelper.insert(newStallData);

    return result;
  }
  //==========================================

  //==========================================
  //Edit Stall Record -> Edit & Update Data from database
  //==========================================
  Future<dynamic> editStallRecord(
      BuildContext context, StallModel object) async {
    print("edit");
    int result = await dbHelper.update(object);
    print("res $result");
    return result;
  }
  //==========================================

  //==========================================
  //Delete Stall Record -> Delete Data from database
  //==========================================
  deleteStallRecord(StallModel object) async {
    int result = await dbHelper.delete(object.id);
    if (result > 0) {
      Fluttertoast.showToast(
          msg: "${object.stallName} Deleted",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.grey,
          fontSize: 12.0);
    }
    return result;
  }
  //==========================================

  //==========================================
  //Check Duplicate -> Only Check by identical name
  //No whitespace or lowercase check
  //==========================================
  Future<bool> checkDuplicate(String name) async {
    bool res = false;
    int result = await dbHelper.checkDuplicate(name);
    if (result > 0) {
      res = true;
    }

    return res;
  }
  //==========================================

  //==========================================
  //Update List View -> Get Stall Data
  //==========================================
  updateListView() {
    print("update List Viewk");
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<StallModel>> stallListFuture = dbHelper.getStallList();
      stallListFuture.then((stallList) {
        this.stallList = stallList;
      });
    });
    return this.stallList;
  }
  //==========================================
}
