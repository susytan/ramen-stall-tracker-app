import 'dart:async';

import 'package:custom_info_window/custom_info_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ramenstall/configs/variables.dart';
import 'package:flutter/foundation.dart';
import 'package:ramenstall/controllers/StallCRUDController.dart';
import 'package:ramenstall/controllers/StallMapController.dart';
import 'package:ramenstall/models/stallModel.dart';
import 'package:clippy_flutter/triangle.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_maps_webservice/places.dart';

class StallMap extends StatefulWidget {
  StallMap({
    Key key,
    this.stallData,
    this.initLatitude,
    this.initLongitude,
  }) : super(key: key);

  final StallModel stallData;
  final double initLatitude;
  final double initLongitude;

  @override
  _StallMapState createState() => _StallMapState();
}

class _StallMapState extends StateMVC<StallMap> {
  //==========================================
  //Variables
  //==========================================
  _StallMapState() : super(StallMapController());
  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  final Set<Marker> _markers = {};
  CustomInfoWindowController _customInfoWindowController =
      CustomInfoWindowController();
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;

  CameraPosition _cameraPosition = CameraPosition(
    target: LatLng(0, 0),
    zoom: mapZoom,
  );

  MapType _defaultMapType = MapType.normal;

  StallModel _stallData;
  double _initLatitude = 0;
  double _initLongitude = 0;
  LatLng tappedLocation;
  String _address = "";
  //==========================================

  //==========================================
  //Init State
  //==========================================
  @override
  void initState() {
    setInitData();

    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }
  //==========================================

  //==========================================
  //Did Change Dependencies
  //==========================================
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  //==========================================

  //==========================================
  //Dispose
  //==========================================
  @override
  void dispose() {
    _customInfoWindowController.dispose();
    super.dispose();
  }
  //==========================================

  //==========================================
  //On Will Pop
  //==========================================
  Future<bool> _onWillPop() async {
    Navigator.pop(context, 'callUpdate');
    return false;
  }
  //==========================================

  //==========================================
  //Set Init State - setup camera position and marker
  //==========================================
  setInitData() {
    if (widget.stallData != null) {
      _stallData = widget.stallData;
    }

    if (widget.initLatitude != null && widget.initLongitude != null) {
      _initLatitude = widget.initLatitude;
      _initLongitude = widget.initLongitude;

      _cameraPosition = CameraPosition(
        target: LatLng(_initLatitude, _initLongitude),
        zoom: mapZoom,
      );

      StallMapController()
          .getAddress(_initLatitude, _initLongitude)
          .then((value) {
        _address = value;
      });

      tappedLocation = LatLng(_initLatitude, _initLongitude);
    }
    setInitMarker();
  }
  //==========================================

  //==========================================
  //Set Init Marker Position & Info window
  //==========================================
  setInitMarker() {
    if (widget.stallData.longitude != null &&
        widget.stallData.latitude != null) {
      _cameraPosition = CameraPosition(
        target: LatLng(widget.stallData.latitude, widget.stallData.longitude),
        zoom: mapZoom,
      );
      tappedLocation =
          LatLng(widget.stallData.latitude, widget.stallData.longitude);

      StallMapController()
          .getAddress(tappedLocation.latitude, tappedLocation.longitude)
          .then((value) {
        _address = value;
      });

      _markers.add(
        Marker(
          markerId: MarkerId(
              "${widget.stallData.latitude}, ${widget.stallData.longitude}"),
          icon: BitmapDescriptor.defaultMarker,
          position:
              LatLng(widget.stallData.latitude, widget.stallData.longitude),
          onTap: () {
            _customInfoWindowController.addInfoWindow(
              addMarker(_address),
              LatLng(tappedLocation.latitude, tappedLocation.longitude),
            );
          },
        ),
      );
      setState(() {});
    }
  }
  //==========================================

  //==========================================
  //Change Map Type
  //==========================================
  void _changeMapType() {
    setState(() {
      _defaultMapType =
          _defaultMapType == MapType.normal ? MapType.hybrid : MapType.normal;
    });
  }
  //==========================================

  //==========================================
  //Add Marker Widget
  //==========================================
  Widget addMarker(String address) {
    return Column(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.food_bank_rounded,
                      color: Colors.black,
                      size: 30,
                    ),
                  ),
                  Flexible(
                    child: Container(
                      color: primaryColor,
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            _stallData.stallName,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            address,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            width: double.infinity,
            height: double.infinity,
          ),
        ),
        Triangle.isosceles(
          edge: Edge.BOTTOM,
          child: Container(
            color: primaryColor,
            width: 20.0,
            height: 10.0,
          ),
        ),
      ],
    );
  }
  //==========================================

  //==========================================
  //Handle Press Button Search (button on top right)
  //==========================================
  Future<void> _handlePressButton() async {
    try {
      //get prediction address
      Prediction p = await PlacesAutocomplete.show(
          context: context,
          apiKey: google_api_key,
          mode: Mode.overlay,
          language: "id",
          components: [new Component(Component.country, "id")]);
      displayPrediction(p, homeScaffoldKey.currentState);
    } catch (e) {
      return;
    }
  }
  //==========================================

  //==========================================
  //Display Prediction
  //==========================================
  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: google_api_key,
        apiHeaders: await GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;

      setState(() {
        _markers.clear();
        _customInfoWindowController.hideInfoWindow();
        tappedLocation = LatLng(lat, lng);
        StallMapController()
            .getAddress(tappedLocation.latitude, tappedLocation.longitude)
            .then((value) {
          _address = value;
        });
        _cameraPosition = CameraPosition(
          target: LatLng(lat, lng),
          zoom: mapZoom,
        );

        mapController.animateCamera(
          CameraUpdate.newCameraPosition(_cameraPosition),
        );

        _markers.add(Marker(
          markerId: MarkerId("${lat}, ${lng}"),
          position: LatLng(lat, lng),
          onTap: () {
            _customInfoWindowController.addInfoWindow(
              addMarker(_address),
              LatLng(lat, lng),
            );
          },
        ));
      });
    }
  }
  //==========================================

  //==========================================
  //Widget Build
  //==========================================
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: primaryColor,
          appBar: AppBar(
            backgroundColor: primaryColor,
            title: Container(
              margin: EdgeInsets.only(right: 50),
              alignment: Alignment.center,
              child: Text(
                appName,
                textAlign: TextAlign.center,
                style: TextStyle(color: defaultHeaderTextColor),
              ),
            ),
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: defaultHeaderTextColor,
              ),
              onPressed: () {
                _onWillPop();
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.black,
                ),
                onPressed: () {
                  _handlePressButton();
                },
              ),
            ],
          ),
          body: Stack(
            children: <Widget>[
              GoogleMap(
                mapType: _defaultMapType,
                initialCameraPosition: _cameraPosition,
                markers: _markers,
                onMapCreated: (GoogleMapController controller) async {
                  _controller.complete(controller);
                  mapController = controller;
                  _customInfoWindowController.googleMapController = controller;
                },
                onCameraMove: (position) {
                  _customInfoWindowController.onCameraMove();
                },
                onTap: (position) {
                  _markers.clear();
                  _customInfoWindowController.hideInfoWindow();
                  tappedLocation = position;
                  StallMapController()
                      .getAddress(
                          tappedLocation.latitude, tappedLocation.longitude)
                      .then((value) {
                    _address = value;
                  });

                  setState(() {
                    _markers.add(
                      Marker(
                        markerId: MarkerId(
                            "${position.latitude}, ${position.longitude}"),
                        position: LatLng(position.latitude, position.longitude),
                        onTap: () {
                          _customInfoWindowController.addInfoWindow(
                            addMarker(_address),
                            LatLng(position.latitude, position.longitude),
                          );
                        },
                      ),
                    );
                  });
                },
                myLocationEnabled: true,
                myLocationButtonEnabled: true,
                rotateGesturesEnabled: true,
                scrollGesturesEnabled: true,
                tiltGesturesEnabled: true,
                zoomGesturesEnabled: true,
              ),
              CustomInfoWindow(
                controller: _customInfoWindowController,
                height: 75,
                width: MediaQuery.of(context).size.width * 0.7,
                offset: 50,
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 10),
                alignment: Alignment.topLeft,
                child: Column(children: <Widget>[
                  FloatingActionButton(
                      child: Icon(
                        Icons.layers,
                        color: Colors.black,
                      ),
                      elevation: 5,
                      backgroundColor: secondaryColor,
                      onPressed: () {
                        _changeMapType();
                      }),
                ]),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  child: Text(
                    "UPDATE",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  color: secondaryColor,
                  onPressed: () async {
                    StallModel passingData = StallModel(
                        id: _stallData.id,
                        stallName: _stallData.stallName,
                        latitude: tappedLocation.latitude,
                        longitude: tappedLocation.longitude);

                    int res = await StallCRUDController()
                        .editStallRecord(context, passingData);
                    if (res > 0) {
                      return showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('Success'),
                            content: Text("Data Updated"),
                            actions: <Widget>[
                              FlatButton(
                                  color: Colors.green,
                                  textColor: Colors.white,
                                  child: Text('OK'),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context, 'callUpdate');
                                  }),
                            ],
                          );
                        },
                      );
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  //==========================================

}
