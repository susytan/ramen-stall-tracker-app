import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ramenstall/configs/variables.dart';
import 'package:flutter/foundation.dart';
import 'package:ramenstall/controllers/HomeController.dart';
import 'package:ramenstall/controllers/StallCRUDController.dart';
import 'package:ramenstall/models/stallModel.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends StateMVC<Home> {
  //==========================================
  //Variables
  //==========================================
  _HomeState() : super(HomeController());
  TextEditingController textFieldController = TextEditingController();
  LatLng currentPosition = LatLng(0, 0);
  //==========================================

  //==========================================
  //Init State
  //==========================================
  @override
  void initState() {
    super.initState();
    HomeController().getCurrentLocation().then((value) {
      currentPosition = value;
    });
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }
  //==========================================

  //==========================================
  //DidChangeDependencies
  //==========================================
  @override
  void didChangeDependencies() {
    StallCRUDController().getAllStallRecord();
    super.didChangeDependencies();
  }
  //==========================================

  //==========================================
  //Dispose
  //==========================================
  @override
  void dispose() {
    textFieldController.dispose();
    super.dispose();
  }
  //==========================================

  //==========================================
  //Display Dialog - Add New Data
  //==========================================
  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('New Stall Record'),
          content: TextFormField(
            controller: textFieldController,
            decoration: InputDecoration(hintText: "Enter Stall Name"),
          ),
          actions: <Widget>[
            FlatButton(
              color: errorColor,
              textColor: Colors.white,
              child: Text('CANCEL'),
              disabledColor: disabledColor,
              onPressed: () {
                setState(() {
                  textFieldController.clear();
                  Navigator.pop(context);
                });
              },
            ),
            FlatButton(
              color: successColor,
              textColor: Colors.white,
              child: Text('OK'),
              disabledColor: disabledColor,
              onPressed: () async {
                if (textFieldController.text.length > 0) {
                  bool dataExists = await StallCRUDController()
                      .checkDuplicate(textFieldController.text ?? "");
                  if (dataExists) {
                    //show alert if data already exists
                    return showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('Failed'),
                            content: Text("Data Already Exists"),
                            actions: <Widget>[
                              FlatButton(
                                  color: successColor,
                                  textColor: Colors.white,
                                  child: Text('OK'),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }),
                            ],
                          );
                        });
                  } else {
                    //data not exists, then add data to database
                    StallCRUDController()
                        .addStallRecord(context, textFieldController.text ?? "",
                            currentPosition)
                        .then((value) {
                      if (value > 0) {
                        StallModel passingData = StallModel(
                            id: value,
                            stallName: textFieldController.text ?? "",
                            latitude: currentPosition.latitude,
                            longitude: currentPosition.longitude);

                        textFieldController.clear();

                        Navigator.pop(context);
                        // move to next page
                        HomeController()
                            .stallMapPage(
                                context,
                                passingData,
                                currentPosition.latitude,
                                currentPosition.longitude)
                            .then(
                          (value) {
                            if (value == "callUpdate") {
                              setState(() {
                                StallCRUDController().updateListView();
                              });
                            }
                          },
                        );
                      }
                    });
                  }
                } else {
                  //text field is empty
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text('Failed'),
                        content: Text("Please Input Stall Name"),
                        actions: <Widget>[
                          FlatButton(
                              color: successColor,
                              textColor: Colors.white,
                              child: Text('OK'),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                        ],
                      );
                    },
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }
  //==========================================

  //==========================================
  //No Data Widget
  //==========================================
  Widget noDataWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 30, bottom: 10, left: 25),
            child: Image.asset(
              'assets/images/no_data.png',
              width: 200,
              height: 200,
            ),
          ),
          Text(
            "No Stall Data",
            style: TextStyle(fontSize: 16),
          )
        ],
      ),
    );
  }
  //==========================================

  //==========================================
  //Widget Build
  //==========================================
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: primaryColor,
        appBar: AppBar(
          backgroundColor: primaryColor,
          title: Container(
            margin: EdgeInsets.only(left: 50),
            alignment: Alignment.center,
            child: Text(
              appName,
              textAlign: TextAlign.center,
              style: TextStyle(color: defaultHeaderTextColor),
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: defaultHeaderTextColor,
              ),
              onPressed: () {
                //show dialog to adding new data
                _displayTextInputDialog(context);
              },
            )
          ],
        ),
        body: FutureBuilder<dynamic>(
          future: StallCRUDController().getAllStallRecord(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Text('');
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.done:
                if (snapshot.hasData) {
                  List<StallModel> data = snapshot.data;
                  if (data.length > 0) {
                    return ListView.builder(
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        final item = data[index];

                        return Slidable(
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          child: GestureDetector(
                              child: ListTile(title: Text('${item.stallName}')),
                              onTap: () {
                                //go to next page
                                HomeController()
                                    .stallMapPage(
                                        context,
                                        item,
                                        currentPosition.latitude,
                                        currentPosition.longitude)
                                    .then(
                                  (value) {
                                    if (value == "callUpdate") {
                                      setState(() {
                                        StallCRUDController().updateListView();
                                      });
                                    }
                                  },
                                );
                              }),
                          secondaryActions: <Widget>[
                            IconSlideAction(
                              caption: 'Delete',
                              color: errorColor,
                              icon: Icons.delete,
                              onTap: () {
                                setState(() {
                                  //slide to delete data
                                  StallCRUDController().deleteStallRecord(item);
                                  StallCRUDController().updateListView();
                                });
                              },
                            ),
                          ],
                        );
                      },
                    );
                  } else {
                    return noDataWidget();
                  }
                } else {
                  return noDataWidget();
                }

                return noDataWidget();
              default:
                return noDataWidget();
            }
          },
        ),
      ),
    );
  }
  //==========================================

}
